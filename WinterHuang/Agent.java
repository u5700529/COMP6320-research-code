package controllers.multiPlayer.WinterHuang;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import core.game.StateObservationMulti;
import core.player.AbstractMultiPlayer;
import ontology.Types;
import ontology.Types.ACTIONS;
import tools.ElapsedCpuTimer;
import tools.Utils;

public class Agent extends AbstractMultiPlayer {
	int oppID;//ID of opponent
	int id;//ID of this player
	int num_players;//number of players in the game
	final static int steplimit = 1;
	
	public Agent(StateObservationMulti stateObs, ElapsedCpuTimer elapsedTimer, int playerID){
		
		num_players = stateObs.getNoPlayers();
		id = playerID; 
		oppID = (playerID+1)%num_players; 
	}

	/**
    *
    *
    * @param stateObs Observation of the current state.
    * @param elapsedTimer Timer when the action returned is due.
    * @return An action for the current state
    */
	@Override
	public Types.ACTIONS act(StateObservationMulti stateObs, ElapsedCpuTimer elapsedTimer) {
		
		Types.ACTIONS bestAction = null;
		double maxEvaluateScore = Double.NEGATIVE_INFINITY;
		double a = Double.NEGATIVE_INFINITY;
		double b = Double.POSITIVE_INFINITY;
		
		ArrayList<ACTIONS> actList = stateObs.getAvailableActions(id);
		Collections.shuffle(actList);
		for(Types.ACTIONS thisAct : actList){
			
			int step = 0;
			StateObservationMulti stCopy = stateObs.copy();
			double evaluateScore = min_score(stCopy, thisAct, step, a, b);
			if(evaluateScore > maxEvaluateScore){
				maxEvaluateScore = evaluateScore;
				bestAction = thisAct;
			}
		}
		
		return bestAction;
	}
	
	public ACTIONS findBestAction(StateObservationMulti stateObs){
		Types.ACTIONS bestAction = null;
		return bestAction;
	}
	
	public double max_score(StateObservationMulti stateObs, int step, double a, double b){
		//When game is over, if player wins and opponent loses, return the score gap with a bonus
		//if player loses and opponent wins, return the score gap with a punish
		//else just return the score gap
		if(stateObs.isGameOver()){
			if(stateObs.getMultiGameWinner()[id] == Types.WINNER.PLAYER_WINS
					&& stateObs.getMultiGameWinner()[id] == Types.WINNER.PLAYER_LOSES)
				return stateObs.getGameScore(id) - stateObs.getGameScore(oppID) - 0.5*step + 1000000000;
			else if(stateObs.getMultiGameWinner()[id] == Types.WINNER.PLAYER_LOSES
					&& stateObs.getMultiGameWinner()[id] == Types.WINNER.PLAYER_WINS)
				return stateObs.getGameScore(id) - stateObs.getGameScore(oppID) - 0.5*step - 1000000000;
			else
				return stateObs.getGameScore(id) - stateObs.getGameScore(oppID) - 0.5*step;
		}
		
		else if(step > steplimit)
			return stateObs.getGameScore(id) - stateObs.getGameScore(oppID);
		
		
		//When game is not over, find the max score
		double max_score = a;
		for(Types.ACTIONS thisAct : stateObs.getAvailableActions(id)){
			StateObservationMulti stCopy = stateObs.copy();
			max_score = Math.max(max_score, min_score(stCopy, thisAct, step, max_score, b));
			if(max_score >= b)
				return max_score;
		}
		
		return max_score;
	}
	
	public double min_score(StateObservationMulti stateObs,Types.ACTIONS thisAct, int step, double a, double b){
		
		//opponent minimize the score
		double min_score = b;
		for(Types.ACTIONS oppAct : stateObs.getAvailableActions(oppID)){
			
			StateObservationMulti stCopy = stateObs.copy();
			Types.ACTIONS[] acts = new Types.ACTIONS[num_players];
			
			acts[id] = thisAct;
			acts[oppID] = oppAct;
			stCopy.advance(acts);
			step++;
			min_score = Math.min(min_score, max_score(stCopy, step, a, min_score));
			if(min_score <= a)
				return min_score;
		}
		return min_score;
	}

}















